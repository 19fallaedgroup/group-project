/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    public void getAverageLikes() {
        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();

        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikesCount.containsKey(user.getId())) {
                    likes = userLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }

        int totalLikes = 0;
        for (int id : userLikesCount.keySet()) {
            totalLikes = totalLikes + userLikesCount.get(id);
        }

        int Comments = 0;
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                Comments++;
            }
        }
        System.out.println("****************************************************");
        System.out.println("Average Number of Likes Per Comment :" + totalLikes / Comments);
    }

    public void getPostMostLikedComment() {
        //post with most liked comments
        // sort posts with likes in comments
        Map<Integer, Integer> postMostLikedComment = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for (Post post : posts.values()) {
            for (Comment c : post.getComments()) {
                int likecounts = 0;
                if (postMostLikedComment.containsKey(post.getPostId())) {
                    likecounts = postMostLikedComment.get(post.getPostId());
                }
                likecounts += c.getLikes();
                postMostLikedComment.put(post.getPostId(), likecounts);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : postMostLikedComment.keySet()) {
            if (postMostLikedComment.get(id) > max) {
                max = postMostLikedComment.get(id);
                maxId = id;
            }
        }
        System.out.println("****************************************************");
        System.out.println("Total Number of Most Liked Comments of Post is:" + max + "\n" + posts.get(maxId));
    }

    public void getPostMostComment() {
        Map<Integer, Integer> postMostComment = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        for (Post post : posts.values()) {
//            for(Comment c: post.getComments()){
            int commentCounts = 0;
            if (postMostComment.containsKey(post.getPostId())) {
                commentCounts = postMostComment.get(post.getPostId());
            }
            commentCounts = post.getComments().size();
            postMostComment.put(post.getPostId(), commentCounts);
        }

        int max = 0;
        int maxId = 0;
        for (int id : postMostComment.keySet()) {
            if (postMostComment.get(id) > max) {
                max = postMostComment.get(id);
                maxId = id;
            }
        }

        System.out.println("****************************************************");
        System.out.println("Total Number of most comments of Post is :" + max + "\n" + posts.get(maxId));
    }

    public void getTopFiveInactiveUsersBasedOnPosts() {
        //Map<Integer, Integer> userPostsCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> userList = new ArrayList<>(users.values());

        for (Post p : posts.values()) {
            userList.get(p.getUserId()).addPostNum();
        }

        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getPostNum() - o2.getPostNum();
            }
        });

        System.out.println("****************************************************");
        System.out.println("Top Five Most Inactive Users Based on Posts:");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i) + " Post Number is : " + userList.get(i).getPostNum());
        }

    }

    public void getTopFiveInactiveUsersBasedOnComments() {

        Map<Integer, User> user = DataStore.getInstance().getUsers();

        List<User> userList = new ArrayList<>(user.values());

        Collections.sort(userList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getComments().size() - o2.getComments().size();
            }
        });
        System.out.println("****************************************************");
        System.out.println("Top Five Most Inactive Users Based on Comments:");
        for (int i = 0; i < userList.size() && i < 5; i++) {
            System.out.println(userList.get(i));
        }

    }
    
    public void getFiveMostInactiveUserOverall() {
        Map<Integer,Integer> userActiveCount = new HashMap<>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        
        List<User> userList = new ArrayList<>(users.values());
        
        List<Post> postList = new ArrayList<>();
        
        for(User user : users.values()) {
            int sum = 0;
            for(Comment c : user.getComments()) {
                if(userActiveCount.containsKey(user.getId())) {
                    sum = userActiveCount.get(user.getId());
                }
                sum = sum + c.getLikes() + 1;
            }
            for(Post p : postList) {
                if(p.getUserId()==user.getId()) {
                    sum++;
                }
            }
            userActiveCount.put(user.getId(), sum);
            System.out.println(sum);
        }
        List<Map.Entry<Integer,Integer>> list = new ArrayList<Map.Entry<Integer,Integer>>(userActiveCount.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer,Integer>>() {
            @Override
            public int compare(Map.Entry<Integer,Integer>o1, Map.Entry<Integer,Integer>o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

        
        System.out.println("****************************************************");
        System.out.println("Top Five Most Inactive Users Based On Sum Of Comments, Posts And Likes:");
        for(int i=0; i<userList.size() && i<5; i++) {
            //System.out.println(list.get(i));
            System.out.println(users.get(list.get(i).getKey()));
        }
    }
    
    public void getFiveMostProactiveUserOverall() {
        Map<Integer,Integer> userActiveCount = new HashMap<>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        
        List<User> userList = new ArrayList<>(users.values());
        
        List<Post> postList = new ArrayList<>();
        
        for(User user : users.values()) {
            int sum = 0;
            for(Comment c : user.getComments()) {
                if(userActiveCount.containsKey(user.getId())) {
                    sum = userActiveCount.get(user.getId());
                }
                sum = sum + c.getLikes() + 1;
            }
            for(Post p : postList) {
                if(p.getUserId()==user.getId()) {
                    sum++;
                }
            }
            userActiveCount.put(user.getId(), sum);
            System.out.println(sum);
        }
        List<Map.Entry<Integer,Integer>> list = new ArrayList<Map.Entry<Integer,Integer>>(userActiveCount.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer,Integer>>() {
            @Override
            public int compare(Map.Entry<Integer,Integer>o1, Map.Entry<Integer,Integer>o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        
        System.out.println("****************************************************");
        System.out.println("Top Five Most Proactive Users Based On Sum Of Comments, Posts And Likes:");
        for(int i=0; i<userList.size() && i<5; i++) {
            //System.out.println(list.get(i));
            System.out.println(users.get(list.get(i).getKey()));
        }
    }

    public void userWithMostLikes() {
        Map<Integer, Integer> userLikesCount = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();

        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikesCount.containsKey(user.getId())) {
                    likes = userLikesCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikesCount.put(user.getId(), likes);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : userLikesCount.keySet()) {
            if (userLikesCount.get(id) > max) {
                max = userLikesCount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most likes :" + max + "\n" + users.get(maxId));
    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {

            @Override
            public int compare(Comment o1, Comment o2) {
                return o2.getLikes() - o1.getLikes();
            }
        });
        System.out.println("5 most liked comments : ");
        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }

    }
}
